﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkMusicBands
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new MusicContext())
            {
                Add add = new Add();
                Show show = new Show();
                Search search = new Search();

                int chosenNumber = 0;
                do
                {
                    Console.Clear();
                    Console.WriteLine("Выберите действие");
                    Console.WriteLine("1. Добавить группу\n2. Добавить песню\n3. Показать все песни\n4. Показать все песни у выбранной группы\n" +
                        "5. Поиск песни по названию\n6. Сортировка по рейтингу\n\nДля выхода нажмите Esc");
                    int key = int.Parse(Console.ReadLine());
                    switch (key)
                    {
                        case 1:
                            Console.WriteLine("Введите имя музыкальной группы");
                            string musicBandName = Console.ReadLine();
                            add.AddMusicBand(musicBandName);
                            break;
                        case 2:
                            Console.WriteLine("Выберите номер музыкальной группы");
                            show.ShowMusicBands();
                            chosenNumber = int.Parse(Console.ReadLine());
                            add.AddSong(chosenNumber);
                            break;
                        case 3:
                            show.ShowSongs();
                            break;
                        case 4:
                            Console.WriteLine("Выберите номер музыкальной группы");
                            show.ShowMusicBands();
                            chosenNumber = int.Parse(Console.ReadLine());
                            search.SearchSongs(chosenNumber);
                            break;
                        case 5:
                            search.SearchSongsName();
                            break;
                        case 6:
                            Console.WriteLine("Выберите сортировку (1 - по убыванию, 2 - по возрастанию)");
                            chosenNumber = int.Parse(Console.ReadLine());
                            search.SortByRating(chosenNumber);
                            break;
                        default: break;
                    }
                } while (Console.ReadKey().Key != ConsoleKey.Escape);
            }
        }
    }
}
