﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkMusicBands
{
    public class Show
    {
        public void ShowMusicBands()
        {
            using (var context = new MusicContext())
            {
                MusicBands musicBands = new MusicBands();
                var musicBandsList = context.MusicBands.ToList();
                Console.WriteLine("{0}. {1}", "N", "Music band name");
                for (int i = 0; i < musicBandsList.Count; i++)
                {
                    Console.WriteLine("{0}. {1}", i+1, musicBandsList.ElementAt(i).Name);
                }
            }
        }

        public void ShowSongs()
        {
            using (var context = new MusicContext())
            {
                Songs songs = new Songs();
                var musicBandsList = context.MusicBands.ToList();
                var songsList = context.Songs.ToList();
                Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 8}", "N", "Song name", "Song time", "Song rating", "Music band name");
                for (int i = 0; i < songsList.Count; i++)
                {
                    Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 15}", i + 1, songsList.ElementAt(i).Name, songsList.ElementAt(i).SongTime,
                        songsList.ElementAt(i).Rating, musicBandsList.Where(j => j.Id == songsList.ElementAt(i).MusicBandId).ToList().ElementAt(0).Name);
                }
            }
        }
    }
}