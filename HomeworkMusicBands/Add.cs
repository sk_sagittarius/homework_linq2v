﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkMusicBands
{
    public class Add
    {
        public void AddMusicBand(string name)
        {
            using (var context = new MusicContext())
            {
                MusicBands musicBands = new MusicBands();
                //Console.WriteLine("Name of music band");
                if(context.MusicBands.Any(i=>i.Name == name))
                {
                    Console.WriteLine("Музыкальная группа с таким названием уже есть");
                    //TODO ссылку на ShowMusicBands
                }
                else
                {
                    musicBands.Name = name;
                    context.MusicBands.Add(musicBands);
                    context.SaveChanges();
                }
            }
        }

        public void AddSong(int number)
        {
            using (var context = new MusicContext())
            {
                Songs songs = new Songs();
                Console.WriteLine("Введите название песни");
                songs.Name = Console.ReadLine();
                Console.WriteLine("Введите текст песни");
                songs.Lyric = Console.ReadLine();
                Console.WriteLine("Введите продожительность песни");
                songs.SongTime = double.Parse(Console.ReadLine());
                Console.WriteLine("Введите рейтинг песни");
                songs.Rating = int.Parse(Console.ReadLine());
                songs.MusicBandId = context.MusicBands.ToList().ElementAt(number - 1).Id;

                context.Songs.Add(songs);
                context.SaveChanges();
            }
        }
    }
}
