﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkMusicBands
{
    public class Search
    {
        public void SearchSongs(int number)
        {
            using (var context = new MusicContext())
            {
                MusicBands musicBands = new MusicBands();
                Songs songs = new Songs();

                var musicBandsList = context.MusicBands.ToList();
                var songsList = context.Songs.ToList();

                var _id = musicBandsList.ElementAt(number - 1).Id;
                Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 8}", "N", "Song name", "Song time", "Song rating", "Music band name");
                for (int i = 0; i < songsList.Count; i++)
                {
                    if (songsList.ElementAt(i).MusicBandId == _id)
                    {
                        Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 15}", i + 1, songsList.ElementAt(i).Name, songsList.ElementAt(i).SongTime,
                        songsList.ElementAt(i).Rating, musicBandsList.Where(j => j.Id == songsList.ElementAt(i).MusicBandId).ToList().ElementAt(0).Name);
                    }
                }
            }
        }

        public void SearchSongsName()
        {
            using (var context = new MusicContext())
            {
                Console.WriteLine("Введите название песни");
                string _name = Console.ReadLine().ToLower(); ;
                var songsList = context.Songs.ToList();
                var musicBandsList = context.MusicBands.ToList();
                Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 8}", "N", "Song name", "Song time", "Song rating", "Music band name");
                for (int i = 0; i < songsList.Count; i++)
                {
                    if(songsList.ElementAt(i).Name.ToLower().Contains(_name))
                    {
                        Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 15}", i + 1, songsList.ElementAt(i).Name, songsList.ElementAt(i).SongTime,
                        songsList.ElementAt(i).Rating, musicBandsList.Where(j => j.Id == songsList.ElementAt(i).MusicBandId).ToList().ElementAt(0).Name);
                    }
                }
            }
        }

        public void SortByRating(int key)
        {
            using (var context = new MusicContext())
            {
                var songsList = context.Songs.ToList();
                var musicBandsList = context.MusicBands.ToList();
                int i = 1;

                if (key == 1)
                {
                    var songsListSorted = from s in songsList orderby s.Rating descending select s;
                    foreach (Songs s in songsListSorted)
                    {
                        Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 15}", i++, s.Name, s.SongTime,
                            s.Rating, musicBandsList.Where(j => j.Id == s.MusicBandId).ToList().ElementAt(0).Name);
                    }
                }
                else if(key ==2)
                {
                    var songsListSorted = from s in songsList orderby s.Rating ascending select s;
                    foreach (Songs s in songsListSorted)
                    {
                        Console.WriteLine("{0}. {1, 14} {2, 15} \t{3, 8} \t{4, 15}", i++, s.Name, s.SongTime,
                            s.Rating, musicBandsList.Where(j => j.Id == s.MusicBandId).ToList().ElementAt(0).Name);
                    }
                }
               
            }
        }
    }
}
